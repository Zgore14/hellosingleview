//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Pierre Zawadil on 2018-10-24.
//  Copyright © 2018 Pierre Zawadil. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func buttonPressed(_ sender: Any) {
        print("hello button")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

